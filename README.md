# Oh My ZSH Setup Instructions

1) Přehodit na ZHS `chsh -s /bin/zsh`
2) Nainstalovat Oh My Zhs `curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh`
3) Upravit soubor `.zshrc`, upravit ZSH_THEME="agnoster"
4) Nainstalovat fonty tim že stáhneš balík, https://github.com/powerline/powerline
5) Jít do nastavení terminálu a upravit font pro terminál na "MESLO LG S Regular for Powerline 11b"

